#include "toggleLED.h"

ToggleLED::ToggleLED(){
	//TODO
	//- Assign Input Output Directions - Digital

	// Set intial state to OFF
	redVal = FALSE;
	blueVal = FALSE;
	greenVal = FALSE;
	
	//Open GPIO Pins 
	exportFile.open("sys/class/gpio/export");
	exportFile << redPin;
	exportFile << bluePin;
	exportFile << greenPin;
	exportFile.close();

	redPinFileD.open("sys/class/gpio/gpio13/direction");
	redPinFileD<<"in";
	bluePinFileD.open("sys/class/gpio/gpio15/direction");
	bluePinFileD<<"in";
	greenPinFileD.open("sys/class/gpio/gpio17/direction");
	greenPinFileD<<"in";

}
ToggleLED::~ToggleLED(){

	unExportFile.open("sys/class/gpio/unexport");
	unExportFile<<redPin;
	unExportFile<<bluePin;
	unExportFile<<greenPin;
	unExportFile.close();
	//TODO :CLose all pinDs and pinVals


}

void ToogleLED::setRedPin(bool state){
	redPinFileVal.open("sys/class/gpio13/value");
	redPinFileVal<<(int)state;
	redPinFileVal.close("sys/class/gpio13/value");

}

void ToogleLED::setBluePin(bool state){
	bluePinFileVal.open("sys/class/gpio17/value");
	bluePinFileVal<<(int)state;
	bluePinFileVal.close("sys/class/gpio17/value");

}

void ToogleLED::setGreenPin(bool state){
	greenPinFileVal.open("sys/class/gpio17/value");
	greenPinFileVal<<(int)state;
	greenPinFileVal.close("sys/class/gpio17/value");

}
void ToggleLED::togglePins(){
	redVal = !redVal;
	blueVal = !blueVal;
	greenVal = !greenVal;

	//Assign Vals to Pins
	setRedPin(redVal);
	setBluePin(blueVal);
	setGreenPin(greenVal);
}