class ToggleLED{
	public:
		ToggleLED();
		~ToggleLED();

		void togglePins();
	private:
		const int redPin = 13;
		const int bluePin = 15;
		const int grennPin = 17;
		bool redVal, blueVal, greenVal;
		
		ofstream exportFile,unExportFile; //Write only
		fstream redPinFileD,redPinFileVal;
		fstream bluePinFileD,bluePinFileVal;
		fstream greenPinFileD,greenPinFileVal; //Read and write

		void setRedPin(bool state);
		void setBluePin(bool state);
		void setGreenPin(bool state);
}